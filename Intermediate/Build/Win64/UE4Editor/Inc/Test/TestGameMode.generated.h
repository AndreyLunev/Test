// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TEST_TestGameMode_generated_h
#error "TestGameMode.generated.h already included, missing '#pragma once' in TestGameMode.h"
#endif
#define TEST_TestGameMode_generated_h

#define Test_Source_Test_TestGameMode_h_12_SPARSE_DATA
#define Test_Source_Test_TestGameMode_h_12_RPC_WRAPPERS
#define Test_Source_Test_TestGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Test_Source_Test_TestGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATestGameMode(); \
	friend struct Z_Construct_UClass_ATestGameMode_Statics; \
public: \
	DECLARE_CLASS(ATestGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Test"), TEST_API) \
	DECLARE_SERIALIZER(ATestGameMode)


#define Test_Source_Test_TestGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesATestGameMode(); \
	friend struct Z_Construct_UClass_ATestGameMode_Statics; \
public: \
	DECLARE_CLASS(ATestGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Test"), TEST_API) \
	DECLARE_SERIALIZER(ATestGameMode)


#define Test_Source_Test_TestGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	TEST_API ATestGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATestGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(TEST_API, ATestGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATestGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	TEST_API ATestGameMode(ATestGameMode&&); \
	TEST_API ATestGameMode(const ATestGameMode&); \
public:


#define Test_Source_Test_TestGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	TEST_API ATestGameMode(ATestGameMode&&); \
	TEST_API ATestGameMode(const ATestGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(TEST_API, ATestGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATestGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATestGameMode)


#define Test_Source_Test_TestGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define Test_Source_Test_TestGameMode_h_9_PROLOG
#define Test_Source_Test_TestGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Test_Source_Test_TestGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	Test_Source_Test_TestGameMode_h_12_SPARSE_DATA \
	Test_Source_Test_TestGameMode_h_12_RPC_WRAPPERS \
	Test_Source_Test_TestGameMode_h_12_INCLASS \
	Test_Source_Test_TestGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Test_Source_Test_TestGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Test_Source_Test_TestGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	Test_Source_Test_TestGameMode_h_12_SPARSE_DATA \
	Test_Source_Test_TestGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Test_Source_Test_TestGameMode_h_12_INCLASS_NO_PURE_DECLS \
	Test_Source_Test_TestGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TEST_API UClass* StaticClass<class ATestGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Test_Source_Test_TestGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
